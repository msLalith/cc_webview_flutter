import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';

class WebView extends StatefulWidget {
  @override
  _WebViewState createState() => _WebViewState();
}

class _WebViewState extends State<WebView> {
  static const String URL = 'https://www.google.com';
  InAppWebViewController _controller;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            ElevatedButton(
              onPressed: () async {
                if (await _controller.canGoBack()) _controller.goBack();
              },
              child: Text('Go Back'),
            ),
            ElevatedButton(
              onPressed: () {
                _controller.loadUrl(url: URL);
              },
              child: Text('Open Google'),
            ),
          ],
        ),
        Expanded(
          child: InAppWebView(
            onWebViewCreated: (controller) => _controller = controller,
            initialOptions: InAppWebViewGroupOptions(
              crossPlatform: InAppWebViewOptions(
                useShouldOverrideUrlLoading: true,
                debuggingEnabled: false,
              ),
            ),
            shouldOverrideUrlLoading: _shouldOverrideUrlLoading,
          ),
        ),
      ],
    );
  }

  Future<ShouldOverrideUrlLoadingAction> _shouldOverrideUrlLoading(
      InAppWebViewController controller,
      ShouldOverrideUrlLoadingRequest shouldOverrideUrlLoadingRequest,
      ) async {
    String url = shouldOverrideUrlLoadingRequest.url;
    if (url.startsWith('https://')) url = url.replaceFirst('https://', '');
    if (url.startsWith('http://')) url = url.replaceFirst('http://', '');
    url = url.split('/')[0];
    print('URL: $url');

    if (url == 'www.samsung.com') return ShouldOverrideUrlLoadingAction.CANCEL;
    return ShouldOverrideUrlLoadingAction.ALLOW;
  }
}