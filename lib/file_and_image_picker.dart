import 'dart:io' show File;

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class FileAndImagePicker extends StatefulWidget {
  @override
  _FileAndImagePickerState createState() => _FileAndImagePickerState();
}

class _FileAndImagePickerState extends State<FileAndImagePicker> {
  final ImagePicker _imagePicker = ImagePicker();
  File _pickedFile;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            ElevatedButton(
              onPressed: _pickFile,
              child: Text('Pick File'),
            ),
            ElevatedButton(
              onPressed: _pickImage,
              child: Text('Pick Image'),
            ),
          ],
        ),
        if (_pickedFile != null)
          Text(_pickedFile.path)
      ],
    );
  }

  void _pickFile() async {
    final FilePickerResult result =
        await FilePicker.platform.pickFiles(
          type: FileType.custom,
          allowedExtensions: <String>['jpg', 'pdf']
        );
    if (result != null) {
      setState(() {
        _pickedFile = File(result.files.single.path);
      });
    }
  }

  void _pickImage() async {
    final PickedFile file =
        await _imagePicker.getImage(source: ImageSource.gallery);
    if (file != null) {
      setState(() {
        _pickedFile = File(file.path);
      });
    }
  }
}
