import 'dart:collection' show LinkedHashMap;

import 'package:flutter/material.dart' show ChangeNotifier;

class PasswordConstraintsNotifier extends ChangeNotifier {
  LinkedHashMap<String, bool> _constraints = LinkedHashMap.from(
    <String, bool>{
      '8 characters': false,
      '1 special character': false,
      '1 lowercase letter': false,
      '1 uppercase letter': false,
      '1 number': false,
    },
  );
  LinkedHashMap<String, bool> get constraints => _constraints;

  void updateAllConstraints(Map<String, bool> constraints) {
    if (_constraints != constraints) {
      _constraints = LinkedHashMap.from(constraints);
      notifyListeners();
    }
  }

  void updateConstraintsOf(String constraint, bool enabled) {
    final localConstraints = LinkedHashMap.from(_constraints);
    if (localConstraints[constraint] != enabled) {
      localConstraints.update(constraint, (value) => enabled);
      _constraints = LinkedHashMap.from(localConstraints);
      notifyListeners();
    }
  }
}
