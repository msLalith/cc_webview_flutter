import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:webview_debugging/file_and_image_picker.dart';
import 'package:webview_debugging/password_constraints_notifier.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Webview Debugging',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: ChangeNotifierProvider(
        create: (context) => PasswordConstraintsNotifier(),
        child: HomePage(),
      ),
    );
  }
}

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Webview Debugging'),
        ),
        body: FileAndImagePicker(),
      ),
    );
  }
}
