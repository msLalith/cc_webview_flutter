import 'dart:collection' show LinkedHashMap;

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:webview_debugging/password_constraints_notifier.dart';

const _horizontalSpacing = EdgeInsets.symmetric(horizontal: 12.0);

class PasswordConstraintsWidget extends StatelessWidget {
  const PasswordConstraintsWidget({
    Key key,
    this.enableBorder = true,
  }) : super(key: key);

  final bool enableBorder;

  @override
  Widget build(BuildContext context) {
    final LinkedHashMap<String, bool> constraints = context
        .select<PasswordConstraintsNotifier, LinkedHashMap<String, bool>>(
      (provider) => provider.constraints,
    );

    final bool hideDisabled =
        constraints.values.where((value) => value == true).length > 2;

    return Padding(
      padding: _horizontalSpacing,
      child: Container(
        padding: enableBorder ? _horizontalSpacing * 2 : EdgeInsets.zero,
        decoration: BoxDecoration(
          border: enableBorder ? Border.symmetric(
            vertical: BorderSide(
              color: Colors.black,
              width: 1.0,
            ),
          ) : null,
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _header(),
            Wrap(
              runSpacing: 0.0, // vertical
              children: [
                for (MapEntry entry in constraints.entries)
                  _constraint(
                    entry.key,
                    entry.value,
                    hideDisabled,
                    () {
                      context
                          .read<PasswordConstraintsNotifier>()
                          .updateConstraintsOf(entry.key, !entry.value);
                    },
                  )
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget _header() {
    return Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: 8.0,
        vertical: 12.0,
      ),
      child: Text(
        'Please use at least:',
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
        style: TextStyle(color: Colors.grey),
      ),
    );
  }

  Widget _constraint(
    String text,
    bool enabled,
    bool hideDisabled,
    VoidCallback onPressed,
  ) {
    if (!enabled && hideDisabled) return const SizedBox.shrink();

    final Widget icon = enabled
        ? Icon(Icons.done, color: Colors.green, size: 16.0)
        : Icon(Icons.close, color: Colors.red, size: 16.0);

    return RawChip(
      backgroundColor: Colors.transparent,
      padding: EdgeInsets.zero,
      avatar: icon,
      elevation: 0.0,
      label: Text(
        text,
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
      ),
      labelStyle: TextStyle(color: Colors.grey),
      onPressed: onPressed,
    );
  }
}
